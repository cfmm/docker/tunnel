#!/bin/sh

#set -x

NAME=tunnel
IP=127.0.1.100
PORT=5022

# Check if container exists
CONTAINER_ID=`docker ps -f name=$NAME -q -a`

DNS_SERVER=`scutil --dns | grep -m 1 'nameserver\[0\]' | awk '{print $3}'`

make_container()
{
   CONTAINER_ID=`docker run --detach --publish $PORT:22 --name $NAME --hostname $NAME --dns $DNS_SERVER --volume ${HOME}/.ssh/id_rsa.pub:/root/.ssh/authorized_keys:ro registry.gitlab.com/cfmm/docker/tunnel:latest`
}

if [ -z "${CONTAINER_ID}" ]; then
   # Create and start if it does not exist
   make_container
else
   if docker inspect --format='{{.HostConfig.Dns}}' $NAME | grep -q -v "$DNS_SERVER"; then
      # Destroy and recreate if the DNS_SERVER is not present
      docker rm -f $CONTAINER_ID 2> /dev/null
      make_container
   else
      # Start if not started
      CONTAINER_ID=`docker ps -f name=$NAME -q`
      if [ -z "${CONTAINER_ID}" ]; then
         docker start $NAME
      fi
   fi
fi

sudo mkdir -p /etc/resolver

for network in `docker network list -f driver=bridge -q --format "{{.Name}}"`
do
   docker network connect $network $NAME 2> /dev/null
   echo "nameserver $IP" | sudo tee /etc/resolver/$network > /dev/null
done

sudo dscacheutil -flushcache 2> /dev/null
sudo killall -HUP mDNSResponder 2> /dev/null
sudo killall mDNSResponderHelper 2> /dev/null
sudo dscacheutil -flushcache

docker exec $NAME rm -f /root/.sshuttle.hosts 2> /dev/null
sshuttle --ssh-cmd "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --auto-hosts --auto-nets --ns-host=$IP  --no-latency-control -r root@127.0.0.1:$PORT

# Cleanup custom resolvers
for network in `docker network list -f driver=bridge -q --format "{{.Name}}"`
do
   if [ $network != "bridge" ]; then
      docker network disconnect $network $NAME 2> /dev/null
      sudo rm -f /etc/resolver/$network
   fi
done
