Usage
=====
On macOS system with [homebrew](https://brew.sh) installed.

```
git clone https://gitlab.com/cfmm/docker/tunnel.git 
homebrew install sshuttle
./tunnel/start_tunnel
```

You will be prompted for password to allow `sudo` access to change the network configuration.

Use Ctrl+C to stop the running tunnel. You will again be prompted for your `sudo` password to revert the network configuration.

Build
=====
Build the container
```
docker build . --tag tunnel
```

Get a list of possible networks
```
docker network list
```

Launch a tunnel into a specific Docker network
```
docker run --detach --publish 5022:22 --name tunnel --hostname tunnel --network dicomaccess_default --dns 10.1.60.2 --dns 10.1.60.3 --dns 129.100.254.134 --dns 129.100.254.135 --dns 8.8.8.8 --volume ${HOME}/.ssh/id_rsa.pub:/root/.ssh/authorized_keys:ro tunnel
```

1. `--dns` must be specified to prevent infinite loop where host queries docker, which queries host, which queries docker, etc. It is recommended to use the primary DNS server from macOS host.
The DNS server needs to be responsive or you will get timeout issues. Using a custom macOS domain resolver configuration will also minimize calls to tunnel.
2. `--network` specifies which network the tunnel allows access. Only bridged containers can publish ports and bridged containers are restrict to there
bridged network. Unlike linux systems a host based network, which allows access to all other containers, cannot publish a port and is not accessible from
the macOS host but only the linux vm host.

The linux vm and `host` based containers use 192.168.65.5 as their DNS server, which does not resolve container names, so you cannot simply add that as a DNS server.

You can add other networks to the `tunnel` using.
```
docker network connect <network_name> <container_name>
docker network connect molecule tunnel
```
This will allow containers on other networks to be resolved. It is advisable to use FQDN (<container_name>.<network_name>) for hosts to ensure you get a container and not actual host. The Docker DNS appears
to check the host resolver (which may append search paths) first. Obviously, the recommended custom macOS domain resolver configuration described below *requires* the use of FQDNs.

It is not possible to specify multiple networks during `run` or `create` of the container, so they have added after creating the container.

Start VPN
=========
```
sshuttle --ssh-cmd "ssh -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null" --ns-host 127.0.1.100 --auto-hosts --auto-nets -r root@localhost:5022
```

1. The `--ns-host` is used to send DNS traffic to the tunnel system. The IP address is not relevant other than that it is an IP address that needs to be added as a DNS server
to the macOS resolver (see below). `sshuttle` will intercept all DNS traffic to that IP and forward the requests to the `tunnel` where they will be resolved by the Docker DNS resolver which
use the internal DNS and the DNS server(s) specified during `tunnel` creation.

`sshuttle` will automatically pick up all the networks that have been added prior to start. It will not tunnel to docker networks started after it launches, so adding a docker network requires 
restarting the tunnel service.

`sshuttle` will cache results in `/root/.sshuttle.hosts`, so if docker changes the IP, e.g. restart docker compose project, you may need to stop `sshutle` and run 

```
docker exec -it tunnel rm /root/.sshuttle.hosts
```

macOS
=====
In order to get macOS to resolve the hosts, it is recommended to add a custom resolver `/etc/resolver/<docker_network_name>` with

```
nameserver 127.0.1.100
```

It is not necessary or advisable to modify the DNS setting in the macOS system preferences. You will need to add a custom resolver for all docker networks that you want to resolve.
You can verify that the resolver is working with `scutil --dns`

```
# scutil --dns
... 
resolver #8
  domain   : dicomaccess_default
  nameserver[0] : 127.0.1.100
  flags    : Request A records
  reach    : 0x00020002 (Reachable,Directly Reachable Address)
...
```

For testing lookup you should `dscacheutil`

```
$  dscacheutil -q host -a name keycloak.dicomaccess_default
name: keycloak.dicomaccess_default
ip_address: 172.18.0.7
```

Tools that do not use macOS system name resolution, e.g. `host`, `nslookup`, and `dig`, will not return valid responses. You can use `nslookup` and `dig` explicit to `127.0.1.100`, but the
custom resolver will never be used implicitly.

Troubleshooting
================
1. Failure to connect with errror:

    ```
    kex_exchange_identification: Connection closed by remote host
    Connection closed by 127.0.0.1 port 5022
    ```

    Rebuilding the docker image, i.e. updating openssh, restored functionality.

