FROM python:3.12.2-alpine3.19

RUN apk add --no-cache --update openssh && rm -rf /tmp/* /var/cache/apk/*

ADD docker-entrypoint.sh /usr/local/bin

EXPOSE 22/tcp
ENTRYPOINT ["/usr/local/bin/docker-entrypoint.sh"]
CMD ["/usr/sbin/sshd", "-D"]
